#!flask/bin/python
import glob
import json

from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route('/dashboard', methods=['GET'])
def get_tasks():
    filename = sorted(glob.glob('output/*.json'))[-1]
    with open(filename) as data_file:
        data = json.load(data_file)
    # return jsonify({'tasks': tasks})
    return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True)