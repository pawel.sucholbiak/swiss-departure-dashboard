# Swiss Dashboard

Live dashboard presenting departures times for Swiss public transport.

Using transportation data from [Transport.opendata.ch](http://transport.opendata.ch/) 

## Installation

Requirements in [requirements.txt](requirements.txt) file

## Running

### Fetching timetables
```bash
python getSchedule.py config-parser.json
```
Example of config file can be found in [config-parser-example.json](config-parser-example.json) file

Information about ```station.location.id``` can be found in documentation of [transport.opendata.ch - locations](http://transport.opendata.ch/docs.html#locations)

### Running server

```bash
export FLASK_APP=SwissDashboard.py
python -m flask run
```

### Client
If the server side is running and timetable data was fetch, just open [client/index.html](client/index.html)

## Author

*Paweł Suchołbiak* <pawel.sucholbiak@gmail.com>  