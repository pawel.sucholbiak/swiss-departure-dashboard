<time>
  <h3>{ current_time }</h3>

  <script>
    get_current_time() {
        var d = new Date();
        var year = d.getFullYear();
        var month = ("0"+(d.getMonth()+1)).slice(-2);
        var day = ("0" + d.getDate()).slice(-2);
        var hour = ("0" + d.getHours()).slice(-2);
        var minutes = ("0" + d.getMinutes()).slice(-2);
        var seconds = ("0" + d.getSeconds()).slice(-2);
        return  year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;

    }
    tick() {
      this.update({ current_time: this.get_current_time() })
    }

    this.current_time = this.get_current_time()

    var time_int = setInterval(this.tick, 1000)

    this.on('unmount', function() {
      clearInterval(time_int)
    })
  </script>

  <style>
    :scope { font-size: 2rem }
    h3 { color: #444 }
    ul { color: #999 }
  </style>
</time>
