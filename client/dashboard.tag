<dashboard>
  <h2 if={opts.error}>Error: {opts.error}</h2>
  <ul>
    <li each={ departures }>{departure_time}: {name}, {destination}</li>
  </ul>

  <script>
    if (opts.error == null){
        this.departures = opts.data.map(row => {
            var d = new Date(row.departureTime);
            var hour = ("0" + d.getHours()).slice(-2);
            var minutes = ("0" + d.getMinutes()).slice(-2);
            return {
                'departure_time': hour+":"+minutes,
                'name': row.category + row.number,
                'destination': row.destination
            };
        });
    }
  </script>
  <style>
    :scope { font-size: 2rem }
    h3 { color: #444 }
    ul { color: #999 }
  </style>
</dashboard>
