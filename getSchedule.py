import json

from datetime import datetime

import os
import requests
import sys

from Journey import Journey
from SbbJsonParser import SbbJsonParser
from jsonschema import validate, ValidationError, SchemaError

CONFIG_PARSER_SCHEMA = 'config-parser.schema'


def get_dashboard_data(config):
    url = config['url']
    raw_dashboard_responses = []

    for station in config['stations']:
        params = dict(
            id=str(station['id']),
            limit=int(station['limit'])
        )
        resp = requests.get(url=url, params=params)
        raw_dashboard_responses.append(json.loads(resp.text))

    journey_list = []
    for data in raw_dashboard_responses:
        journey_list += SbbJsonParser.parse_json(data)

    current_time = datetime.now().strftime("%Y%m%d%H%M%S")

    output_file = open("output/%s.json" % current_time, "w")
    json.dump(journey_list, output_file, indent=4, default=Journey.serialize)
    output_file.close()


def validate_config(config):
    with open(CONFIG_PARSER_SCHEMA) as raw_schema:
        schema = json.load(raw_schema)

    try:
        validate(config, schema)
    except ValidationError as exc:
        print("Config file is invalid: \n%s" % exc)
        exit(1)
    except SchemaError:
        print("Schema is invalid")
        exit(1)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Please provide a path to config file")
        exit(1)

    config_path = sys.argv[1]
    if not os.path.isfile(config_path):
        print("%s - file does not exit" % config_path)
        exit(1)

    with open(config_path) as data_file:
        config = json.load(data_file)

    validate_config(config)
    get_dashboard_data(config)
