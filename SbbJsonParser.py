from datetime import datetime

from Journey import Journey


class SbbJsonParser(object):

    @staticmethod
    def parse_json(data):
        journey_list = []
        for boardEntry in data['stationboard']:
            j = Journey()
            j.category = boardEntry['category']
            j.number = boardEntry['number']
            j.destination = boardEntry['to'].replace('"', '')
            j.departureTime = datetime.fromtimestamp(float(boardEntry['stop']['departureTimestamp']))
            journey_list.append(j)
        return journey_list
