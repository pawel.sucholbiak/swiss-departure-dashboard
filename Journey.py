import json
from datetime import datetime, date, time


class Journey(object):

    def __init__(self):
        self.departureTime = None
        self.category = None
        self.number = None
        self.destination = None
        self.departureTime = None

    def __str__(self):
        return "%s - %s%s to %s" % (self.departureTime.strftime('%Y-%m-%d %T'), self.category, self.number, self.destination)

    @staticmethod
    def serialize(obj):
        """JSON serializer for objects not serializable by default json code"""

        if isinstance(obj, date):
            return obj.isoformat()

        return obj.__dict__

    def to_json(self):
        return json.dumps(self, default=self.serialize)
